package com.example.recyclerview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.*
import java.io.IOException


class LoginActivity : AppCompatActivity() {


    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //SharedPreferences


        back_to_register_text_view.setOnClickListener {
            //Log.d("MainActivity", "show activity")
            val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
            //go to registration
            val userInfoId = sharedPreferences.getInt("ID",0)
            if(userInfoId == 0){
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }else{
                Toast.makeText(this, "You are already logged in", Toast.LENGTH_SHORT).show()
            }
        }

        go_back_text_view.setOnClickListener {

            //go to main page
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        login_button_login.setOnClickListener {
            val email = email_edittext_login.text.toString().trim()
            val password = password_edittext_login.text.toString().trim()

            if(email.isEmpty()){
                email_edittext_login.error = "Email required"
                email_edittext_login.requestFocus()
                return@setOnClickListener
            }
            if(password.isEmpty()){
                password_edittext_login.error = "Password required"
                password_edittext_login.requestFocus()
                return@setOnClickListener
            }

            val client = OkHttpClient()

            val formBody = FormBody.Builder()
                .add("email", email)
                .add("password", password)
                .build()
            val request = Request.Builder()
                .url("https://playformeproject.000webhostapp.com/login-json.php")
                .post(formBody)
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){
                    val body = response.body?.string()
                    println(body)
                    val gson = GsonBuilder().create()
                    val userInfo = gson.fromJson(body, User::class.java)
                    if(userInfo.email.isNullOrEmpty())
                    {
                        val failInfo = gson.fromJson(body, UserResponse::class.java)
                        println(failInfo.message)
                        //Toast.makeText(this@LoginActivity, failInfo.message, Toast.LENGTH_SHORT).show()
                    }else
                    {
                        val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
                        //save data to device storage
                        val editor = sharedPreferences.edit()
                        editor.clear()
                        editor.putInt("ID", userInfo.id)
                        editor.putString("EMAIL", userInfo.email)
                        editor.putString("USERNAME", userInfo.username)
                        editor.putString("PHONE", userInfo.phone)
                        editor.putString("AVATAR", userInfo.avatar)
                        editor.putInt("SUB", userInfo.subscription)
                        println("Podczas logowania admin: "+userInfo.is_admin)
                        editor.apply()

                        //go to main page
                        //Toast.makeText(this@LoginActivity, "Welcome ${userInfo.username}", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
            })


        }

    }


}
