package com.example.recyclerview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_buy_premium.*
import com.paypal.android.sdk.payments.PayPalConfiguration
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.paypal.android.sdk.payments.PayPalService
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.paypal.android.sdk.payments.PaymentActivity
import com.paypal.android.sdk.payments.PayPalPayment
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.math.BigDecimal
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.paypal.android.sdk.i
import android.app.Activity
import android.content.Context
import com.paypal.android.sdk.e
import org.json.JSONException
import com.paypal.android.sdk.payments.PaymentConfirmation
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import android.widget.Toast
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException


val PAYPAL_REQUEST_CODE: Int = 7171

val PAYPAL_CLIENT_ID: String = "ARy2q8a9y5HP7yK_msRTN4OAjPi3kjHL_ORP4g_kQe7o-DG4Wjf3Lz880O-Y3o7K6N6r2QeHegrUAnt4"
val config = PayPalConfiguration()

    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
    // or live (ENVIRONMENT_PRODUCTION)
    .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)

    .clientId(PAYPAL_CLIENT_ID)


class BuyPremiumActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_premium)


        pay_buy_premium_button.setOnClickListener{
            processPayment()
        }

        go_back_buy_premium.setOnClickListener {
            val intentgo = Intent(this, ProfileActivity::class.java)
            startActivity(intentgo)
        }


    }

    fun processPayment()
    {
        val payment = PayPalPayment(
            BigDecimal("1.75"), "USD", "Premium",
            PayPalPayment.PAYMENT_INTENT_SALE
        )

        val intent = Intent(this, PaymentActivity::class.java)

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment)

        startActivityForResult(intent, 0)
    }

    fun addSub(id: String) {
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("user_id", id)
            .build()

        val url = "https://playformeproject.000webhostapp.com/subscription-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val resp = gson.fromJson(body, UserResponse::class.java)
                println(resp)
            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val confirm =
                data!!.getParcelableExtra<PaymentConfirmation>(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4))

                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details

                    // ADD SUB TO DATABASE
                    val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
                    val userInfoId = sharedPreferences.getInt("ID", 0)

                    addSub(userInfoId.toString())

                    val editor = sharedPreferences.edit()
                    editor.remove("SUB")
                    editor.putInt("SUB", 1)
                    editor.apply()

                    Toast.makeText(this, "Payment Complete", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, ProfileActivity::class.java)
                    startActivity(intent)

                } catch (e: JSONException) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e)
                }

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.")
            Toast.makeText(this, "Payment Canceled", Toast.LENGTH_SHORT).show()
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i(
                "paymentExample",
                "An invalid Payment or PayPalConfiguration was submitted. Please see the docs."
            )
            Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onDestroy() {
        stopService(Intent(this, PayPalService::class.java))
        super.onDestroy()
    }
}
