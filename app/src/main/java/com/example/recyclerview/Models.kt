package com.example.recyclerview

class HomeFeed(val posts:List<Post>)

class Config(val PAYPAL_CLIENT_ID: String = "ARy2q8a9y5HP7yK_msRTN4OAjPi3kjHL_ORP4g_kQe7o-DG4Wjf3Lz880O-Y3o7K6N6r2QeHegrUAnt4")

class Post(val id: Int,
           val user_id: Int,
           val category: String,
           val title: String,
           val description: String,
           val image: String,
           val date: String)

class User(val id: Int,
           val username: String,
           val email: String,
           val password: String,
           val phone: String,
           val avatar: String,
           val subscription: Int,
           val is_admin: Int)

class SinglePost(val post: Post,
                 val user: User)

data class UserResponse(val error: Boolean,
                            val message: String)