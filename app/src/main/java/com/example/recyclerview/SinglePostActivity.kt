package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_single_post.*
import okhttp3.*
import java.io.IOException

class SinglePostActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_post)

        var post_id: String = intent.getStringExtra("post_id")

        fetchJson(post_id)
    }

    fun fetchJson(post_id: String) {
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("post_id", post_id)
            .build()

        val url = "https://playformeproject.000webhostapp.com/post-user-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)


                val gson = GsonBuilder().create()
                val post = gson.fromJson(body, SinglePost::class.java)
                runOnUiThread {
                    val thumbnailImageView = imageView_profile_thumbnail
                    Picasso.get().load(post.post.image).into(thumbnailImageView)

                    textView_profile_title.text = post.post.title
                    textView_profile_username.text = post.user.username
                    textView_profile_email.text = post.user.email
                    textView_profile_phone.text = post.user.phone

                    val channelProfileImageView = imageView_profile_avatar
                    Picasso.get().load(post.user.avatar).into(channelProfileImageView)

                    textView_profile_date.text = post.post.date
                    textView_profile_description.text = post.post.description
                }


            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
}
