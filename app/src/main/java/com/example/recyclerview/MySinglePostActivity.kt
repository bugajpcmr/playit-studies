package com.example.recyclerview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_add_post.go_back_text_view
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.spinner_categories_edit
import kotlinx.android.synthetic.main.activity_my_single_post.*
import kotlinx.android.synthetic.main.activity_single_post.*
import okhttp3.*
import java.io.IOException

class MySinglePostActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_single_post)

        val post_id: String = intent.getStringExtra("post_id")
        var title: String = intent.getStringExtra("title")
        var desc: String = intent.getStringExtra("description")
        var image: String = intent.getStringExtra("image")
        val category: String = intent.getStringExtra("category")


        title_edittext_edit.setText(title)
        desc_EditText_edit.setText(desc)
        image_edittext_edit.setText(image)

        //spinner setup
        val list: MutableList<String> = ArrayList()

        list.add("other")
        list.add("DJ")
        list.add("band")
        list.add("standupper")

        val sp_adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, list)
        spinner_categories_edit.adapter = sp_adapter
        var itemCat: String = category
        spinner_categories_edit.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                itemCat = list[position]
                if(position>0)
                    Toast.makeText(this@MySinglePostActivity, "$itemCat selected", Toast.LENGTH_SHORT).show()
            }

        }

        //apply
        add_button_edit.setOnClickListener {
            title = title_edittext_edit.text.toString().trim()
            desc = desc_EditText_edit.text.toString().trim()
            image = image_edittext_edit.text.toString().trim()
            fetchJson(post_id, itemCat, title, desc, image)
        }

        //back to main
        go_back_text_view.setOnClickListener {

            //go to main page
            val intent = Intent(this, MyPostsActivity::class.java)
            startActivity(intent)
        }

    }
    fun fetchJson(post_id: String, category: String, title: String, description: String, image: String) {
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("post_id", post_id)
            .add("category", category)
            .add("title", title)
            .add("description", description)
            .add("image", image)
            .build()

        val url = "https://playformeproject.000webhostapp.com/edit-post-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val userInfo = gson.fromJson(body, UserResponse::class.java)

                if(userInfo.error) {
                    println("${userInfo.message}...")
                }else
                {
                    println(userInfo.message)
                    val intent = Intent(this@MySinglePostActivity, MyPostsActivity::class.java)
                    startActivity(intent)
                }

            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
}
