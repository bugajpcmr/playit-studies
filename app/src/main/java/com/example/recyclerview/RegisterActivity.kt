package com.example.recyclerview

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.add_button_add
import kotlinx.android.synthetic.main.activity_register.go_back_text_view
import kotlinx.android.synthetic.main.activity_register.image_edittext_add
import okhttp3.*
import java.io.IOException

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        already_have_account_text_view.setOnClickListener {

            //go to login
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        go_back_text_view.setOnClickListener {

            //go to main page
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }



        terms_of_usage_text_view.setOnClickListener {


            //alert
            val builder = AlertDialog.Builder(this)

            builder.setTitle("Terms of service")
            builder.setMessage("These Terms and Conditions constitute a legally binding agreement made between you, whether personally or on behalf of an entity (“you”) and [your business name] (“we,” “us” or “our”), concerning your access to and use of our mobile application (the “Application”). You agree that by accessing the Application, you have read, understood, and agree to be bound by all of these Terms and Conditions Use. IF YOU DO NOT AGREE WITH ALL OF THESE TERMS AND CONDITIONS, THEN YOU ARE EXPRESSLY PROHIBITED FROM USING THE APPLICATION AND YOU MUST DISCONTINUE USE IMMEDIATELY. ")

            builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                // Delete post

                ContextCompat.startActivity(this, intent, Bundle(0))

                //Toast.makeText(this, "", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
            })

            val alert = builder.create()
            alert.show()
        }

        add_button_add.setOnClickListener {

            val username = username_edittext_register.text.toString().trim()
            val email = title_edittext_edit.text.toString().trim()
            val password = desc_edittext_add.text.toString().trim()
            val phone = image_edittext_add.text.toString().trim()

            if(username.isEmpty()){
                username_edittext_register.error = "Username required"
                username_edittext_register.requestFocus()
                return@setOnClickListener
            }
            if(email.isEmpty()){
                title_edittext_edit.error = "Email required"
                title_edittext_edit.requestFocus()
                return@setOnClickListener
            }
            if(password.isEmpty()){
                desc_edittext_add.error = "Password required"
                desc_edittext_add.requestFocus()
                return@setOnClickListener
            }
            if(phone.isEmpty()){
                image_edittext_add.error = "Phone number required"
                image_edittext_add.requestFocus()
                return@setOnClickListener
            }
            if(!checkBox_agree.isChecked)
            {
                checkBox_agree.error = "You need to agree"
                checkBox_agree.requestFocus()
                return@setOnClickListener
            }


            //POST request

            val client = OkHttpClient()

            val formBody = FormBody.Builder()
                .add("username", username)
                .add("email", email)
                .add("password", password)
                .add("phone", phone)
                .build()
            val request = Request.Builder()
                .url("https://playformeproject.000webhostapp.com/register-json.php")
                .post(formBody)
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){
                    val body = response.body?.string()
                    println(body)
                    val gson = GsonBuilder().create()
                    val registerInfo = gson.fromJson(body, UserResponse::class.java)
                    if(registerInfo.error)
                    {
                        println(registerInfo.message)
                    }else
                    {
                        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                        startActivity(intent)
                    }

                }
            })

        }
    }
}
