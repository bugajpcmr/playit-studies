package com.example.recyclerview

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_add_post.add_button_add
import kotlinx.android.synthetic.main.activity_add_post.image_edittext_add
import kotlinx.android.synthetic.main.activity_add_post.title_edittext_add
import okhttp3.*
import java.io.IOException


class AddPostActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        //spinner setup
        val list: MutableList<String> = ArrayList()

        list.add("other")
        list.add("DJ")
        list.add("band")
        list.add("standupper")

        val sp_adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, list)
        spinner_categories_add.adapter = sp_adapter
        var itemCat: String = "other"
        spinner_categories_add.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                itemCat = list[position]
                if(position>0)
                    Toast.makeText(this@AddPostActivity, "$itemCat selected", Toast.LENGTH_SHORT).show()
            }

        }

        //back to main
        go_back_text_view.setOnClickListener {

            //go to main page
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        //add post button
        add_button_add.setOnClickListener{
            val title = title_edittext_add.text.toString().trim()
            val description = desc_EditText_add.text.toString().trim()
            var image = image_edittext_add.text.toString().trim()

            if(title.isEmpty()){
                title_edittext_add.error = "Title required"
                title_edittext_add.requestFocus()
                return@setOnClickListener
            }
            if(description.isEmpty()){
                desc_EditText_add.error = "Description required"
                desc_EditText_add.requestFocus()
                return@setOnClickListener
            }
            if(image.isEmpty()){
                image = "http://silkbrassband.co.uk/images/no-image-selected.png"
            }

            fetchJson(itemCat, title, description, image)
        }
    }
    fun fetchJson(category: String, title: String, description: String, image: String) {
        println("Attemting to fetch JSON")

        val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
        val userInfoId = sharedPreferences.getInt("ID", 0)
        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("user_id", userInfoId.toString())
            .add("category", category)
            .add("title", title)
            .add("description", description)
            .add("image", image)
            .build()

        val url = "https://playformeproject.000webhostapp.com/add-post-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val userResponse = gson.fromJson(body, UserResponse::class.java)

                if(userResponse.error)
                {
                    println("Unable to add post due to: "+userResponse.message)
                }else
                {
                    val intent = Intent(this@AddPostActivity, MainActivity::class.java)
                    startActivity(intent)
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
}
