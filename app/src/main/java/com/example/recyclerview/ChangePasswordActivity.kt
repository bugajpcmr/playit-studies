package com.example.recyclerview

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_login.password_edittext_login
import okhttp3.*
import java.io.IOException

class ChangePasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        go_back_text_view.setOnClickListener {
            val intent = Intent(this@ChangePasswordActivity, ProfileActivity::class.java)
            startActivity(intent)
        }

        login_button_login.setOnClickListener{
            //get user ID
            val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
            val userInfoId = sharedPreferences.getInt("ID",0)

            //get phone and password from edittext
            val phone = password_edittext_login.text.toString().trim()

            val password1 = newpassword1_edittext_change.text.toString().trim()
            val password2 = newpassword2_edittext_change.text.toString().trim()
            if(password1 != password2){
                Toast.makeText(this, "Passwords are different", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else{
                changeUserInfo(userInfoId.toString(), phone, password1)
            }
        }

    }

    fun changeUserInfo(id: String, phone: String, password: String) {
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("user_id", id)
            .add("phone", phone)
            .add("password", password)
            .add("email", "")
            .add("username", "")
            .build()

        val url = "https://playformeproject.000webhostapp.com/edit-user-profile-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val failInfo = gson.fromJson(body, UserResponse::class.java)
                if(failInfo.error)
                {
                    println(failInfo.message)
                }else
                {
                    val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.remove("PHONE")
                    editor.putString("PHONE", phone)
                    editor.apply()
                    val intent = Intent(this@ChangePasswordActivity, ProfileActivity::class.java)
                    startActivity(intent)
                }


            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
}
