package com.example.recyclerview

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.post_row.view.*
import kotlinx.android.synthetic.main.video_row.view.*
import kotlinx.android.synthetic.main.video_row.view.imageView_profile_thumbnail
import kotlinx.android.synthetic.main.video_row.view.textView_profile_date
import kotlinx.android.synthetic.main.video_row.view.textView_profile_description
import kotlinx.android.synthetic.main.video_row.view.textView_profile_title
import okhttp3.*
import java.io.IOException
import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity


class MyPostsAdapter(val homeFeed: HomeFeed): RecyclerView.Adapter<CustomHolder>() {

    //val postTitles = listOf<String>("First title", "Second Title", "Third Title", "Forth Title", "Fifth Title")

    //numberOfItems
    override fun getItemCount(): Int {
        return homeFeed.posts.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
        //creating a view
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.post_row, parent, false)
        return CustomHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomHolder, position: Int) {
//        val videoTitle = videoTitles.get(position)
        val post = homeFeed.posts.get(position)

        holder.view.textView_profile_title.text = post.title
        holder.view.textView_profile_description.text = "Category: "+post.category
        holder.view.textView_profile_date.text = "Date: "+post.date

        val context = holder.itemView.context
        val intent = Intent(context, MyPostsActivity::class.java)
        //picasso url image
        val thumbnailImageView = holder.view.imageView_profile_thumbnail
        Picasso.get().load(post.image).into(thumbnailImageView)

        //val channelProfileImageView = holder.view.imageView_user_profile
        //Picasso.get().load(post.image).into(channelProfileImageView)

        //delete post
        holder.view.myposts_button_delete.setOnClickListener {

            //alert
            val builder = AlertDialog.Builder(context)

            builder.setTitle("Confirm")
            builder.setMessage("Are you sure?")

            builder.setPositiveButton("YES", DialogInterface.OnClickListener { dialog, which ->
                // Delete post
                deletePost(post.id)

                startActivity(context,intent, Bundle(0))

                Toast.makeText(context, "Post deleted", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
            })

            builder.setNegativeButton("NO", DialogInterface.OnClickListener { dialog, which ->
                // Do nothing
                dialog.dismiss()
            })

            val alert = builder.create()
            alert.show()


        }

        //edit post
        holder.view.myposts_button_edit.setOnClickListener {
            val intent = Intent(holder.view.context, MySinglePostActivity::class.java)
            intent.putExtra("post_id", post.id.toString())
            intent.putExtra("title", post.title)
            intent.putExtra("description", post.description)
            intent.putExtra("image", post.image)
            intent.putExtra("category", post.category)
            holder.view.context.startActivity(intent)
        }

        //view post
        holder.view.setOnClickListener {
            println("Test message")

            val intent = Intent(holder.view.context, SinglePostActivity::class.java)
            intent.putExtra("post_id", post.id.toString())
            holder.view.context.startActivity(intent)
        }
    }


    private fun deletePost(post_id: Int){
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("post_id", post_id.toString())
            .build()

        val url = "https://playformeproject.000webhostapp.com/delete-post.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)



            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }

}
class CustomHolder(val view: View): RecyclerView.ViewHolder(view) {

}