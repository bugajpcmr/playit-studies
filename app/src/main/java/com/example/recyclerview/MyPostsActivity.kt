package com.example.recyclerview

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_my_posts.*
import okhttp3.*
import java.io.IOException

class MyPostsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_posts)

        recyclerView_myposts.layoutManager = LinearLayoutManager(this)

        val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
        val userInfoId = sharedPreferences.getInt("ID", 0)

        go_back_myposts.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }

        fetchJson(userInfoId)


    }
    fun fetchJson(user_id: Int) {
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("user_id", user_id.toString())
            .build()

        val url = "https://playformeproject.000webhostapp.com/user-posts-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val homeFeed = gson.fromJson(body, HomeFeed::class.java)

                runOnUiThread {
                    recyclerView_myposts.adapter = MyPostsAdapter(homeFeed)
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
}
