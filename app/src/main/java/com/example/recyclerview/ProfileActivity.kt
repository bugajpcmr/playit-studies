package com.example.recyclerview

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.view.isVisible
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.video_row.view.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)


        //get user info
        val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)

        val userInfoUsername = sharedPreferences.getString("USERNAME","Username: ")
        val userInfoEmail = sharedPreferences.getString("EMAIL","Email: ")
        val userInfoPhone = sharedPreferences.getString("PHONE","Phone number: ")
        val userInfoAvatar = sharedPreferences.getString("AVATAR","https://www.kindpng.com/picc/m/116-1169050_avatar-michael-jordan-jersey-clip-art-michael-jordan.png")
        val userInfoSub = sharedPreferences.getInt("SUB", 0)
        textView_profile_username.text = "Username: "+userInfoUsername
        textView_profile_email.text = "Email: "+userInfoEmail
        textView_profile_phone.text = "Phone number: "+userInfoPhone
        if(userInfoSub == 1)
        {
            textView_profile_sub.text = "Premium"

        }else
        {
            textView_profile_sub.text = "Non-premium"
            buy_premium_profile_button.visibility = Button.VISIBLE
        }

        //picasso url image
        val thumbnailImageView = imageView_profile_avatar
        Picasso.get().load(userInfoAvatar).into(thumbnailImageView)



        //change user info (phone, password)
        profile_change_userinfo_button.setOnClickListener {
            val intent = Intent(this, ChangePasswordActivity::class.java)
            startActivity(intent)
        }

        buy_premium_profile_button.setOnClickListener {
            val intent = Intent(this, BuyPremiumActivity::class.java)
            startActivity(intent)
        }

        //delete user info from device storage
        profile_logout_button.setOnClickListener {

            val editor = sharedPreferences.edit()
            editor.remove("ID")
            editor.remove("EMAIL")
            editor.remove("SUB")
            editor.remove("PHONE")
            editor.remove("AVATAR")
            editor.remove("USERNAME")
            editor.apply()

            println("user info has been deleted")

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "Logout complete", Toast.LENGTH_SHORT).show()
        }
        profile_home_button.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        profile_my_posts_button.setOnClickListener {
            val intent = Intent(this, MyPostsActivity::class.java)
            startActivity(intent)
        }


    }
}
