package com.example.recyclerview

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.video_row.view.*

class MainAdapter(val homeFeed: HomeFeed): RecyclerView.Adapter<CustomViewHolder>() {

    //val postTitles = listOf<String>("First title", "Second Title", "Third Title", "Forth Title", "Fifth Title")

    //numberOfItems
    override fun getItemCount(): Int {
        return homeFeed.posts.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        //creating a view
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.video_row, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
//        val videoTitle = videoTitles.get(position)
        val post = homeFeed.posts.get(position)

        holder.view.textView_profile_title.text = post.title
        holder.view.textView_profile_description.text = "Category: "+post.category
        holder.view.textView_profile_date.text = "Date: "+post.date

        //picasso url image
        val thumbnailImageView = holder.view.imageView_profile_thumbnail
        Picasso.get().load(post.image).into(thumbnailImageView)

        //val channelProfileImageView = holder.view.imageView_user_profile
        //Picasso.get().load(post.image).into(channelProfileImageView)

        holder.view.setOnClickListener {
            println("Test message")

            val intent = Intent(holder.view.context, SinglePostActivity::class.java)
            intent.putExtra("post_id", post.id.toString())
            holder.view.context.startActivity(intent)
        }
    }
}

class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view) {

}