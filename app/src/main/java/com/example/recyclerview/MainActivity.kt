package com.example.recyclerview

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView_main.layoutManager = LinearLayoutManager(this)



        //check if user is in the database
        authenticateUser()



        //Spinner
        val list: MutableList<String> = ArrayList()

        list.add("choose")
        list.add("DJ")
        list.add("band")
        list.add("standupper")
        list.add("other")

        val sp_adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, list)
        spinner_categories_edit.adapter = sp_adapter
        var itemCat: String = "choose"
        spinner_categories_edit.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                itemCat = list[position]
                if(position>0)
                Toast.makeText(this@MainActivity, "$itemCat selected", Toast.LENGTH_SHORT).show()
            }

        }

        var search = ""

        //JsonPost search
        fetchJson(itemCat, search)

        //onclick
        search_button_main.setOnClickListener {
            search = search_edittext_main.text.toString().trim()
            fetchJson(itemCat, search)
            Toast.makeText(this@MainActivity, "search complete", Toast.LENGTH_SHORT).show()
        }
        //home
        home_button_main.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        //profile
        profile_button_main.setOnClickListener {
            val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
            val userInfoId = sharedPreferences.getInt("ID", 0)
            if(userInfoId == 0)
            {
                //go to login
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }else
            {
                //open profile activity
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
            }
        }
        //add post
        addpost_button_main.setOnClickListener {
            val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
            val userInfoId = sharedPreferences.getInt("ID", 0)
            val userInfoSub = sharedPreferences.getInt("SUB", 0)
            if(userInfoId == 0)
            {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                Toast.makeText(this@MainActivity, "You need to log in", Toast.LENGTH_SHORT).show()
            }else if(userInfoSub == 0)
            {
                Toast.makeText(this@MainActivity, "You need to buy premium", Toast.LENGTH_SHORT).show()
            }else
            {
                val intent = Intent(this, AddPostActivity::class.java)
                startActivity(intent)
                Toast.makeText(this@MainActivity, "Add new post", Toast.LENGTH_SHORT).show()
            }


        }

    }


    fun fetchJson(category: String, search: String) {
        println("Attemting to fetch JSON")

        val client = OkHttpClient()

        val formBody = FormBody.Builder()
            .add("category", category)
            .add("search", search)
            .build()

        val url = "https://playformeproject.000webhostapp.com/list-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val homeFeed = gson.fromJson(body, HomeFeed::class.java)

                runOnUiThread {
                    recyclerView_main.adapter = MainAdapter(homeFeed)
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
    fun authenticateUser() {
        val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
        val client = OkHttpClient()

        val userInfoId = sharedPreferences.getInt("ID", 0)
        val userInfoEmail = sharedPreferences.getString("EMAIL","")

        val formBody = FormBody.Builder()
            .add("user_id", userInfoId.toString())
            .add("email", userInfoEmail!!)
            .build()

        val url = "https://playformeproject.000webhostapp.com/user-authenticate-json.php"
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.string()
                println(body)

                val gson = GsonBuilder().create()
                val userInfo = gson.fromJson(body, UserResponse::class.java)

                if(userInfo.error)
                {
                    println("${userInfo.message}... deleting device storage data")
                    val editor = sharedPreferences.edit()
                    editor.remove("ID")
                    editor.remove("EMAIL")
                    editor.remove("SUB")
                    editor.remove("PHONE")
                    editor.remove("AVATAR")
                    editor.remove("USERNAME")
                    editor.apply()
                    //Toast.makeText(this@LoginActivity, failInfo.message, Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }
}

